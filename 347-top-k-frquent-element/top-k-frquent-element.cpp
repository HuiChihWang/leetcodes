// https://leetcode.com/problems/top-k-frequent-elements/

#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        
        unordered_map<int, int> counts;
        vector<int> numSet;
        for (auto& num: nums) {
            if (counts.find(num) == counts.end()) {
                counts[num] = 0;
                numSet.push_back(num);
            }
            counts[num] += 1;
        }

        quickSelect(counts, numSet, 0, numSet.size() - 1, k);

        return vector<int>(numSet.begin(), numSet.begin() + k);
    }

    void quickSelect(unordered_map<int, int>& counts, vector<int>& numSet, int start, int end, int k) {
        int pivotIndex = partiton(counts, numSet, start, end);

        int leftSize = pivotIndex - start + 1;

        if (leftSize == k) {
            return;
        } else if (leftSize < k) {
            quickSelect(counts, numSet, pivotIndex + 1, end, k - leftSize);
        } else {
            quickSelect(counts, numSet, start, pivotIndex - 1, k);
        }
    }


    int partiton(unordered_map<int, int>& counts, vector<int>& numSet, int start, int end) {
        int pivotIndex = end;
        swap(numSet, pivotIndex, end);
        int pivot = counts[numSet[end]];
        int leftIndex = start - 1;
        for (int i = start; i < end; ++i) {
            int count = counts[numSet[i]];
            if (count >= pivot) {
                swap(numSet, i, ++leftIndex);
            }
        }

        pivotIndex = leftIndex + 1;
        swap(numSet, pivotIndex, end);

        return pivotIndex;
    }

    void swap(vector<int>& arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
};