// https://leetcode.com/problems/move-zeroes/description/

#include <vector>

using namespace std;

class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int left = -1;

        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] != 0) {
                swap(nums, i, ++left);
            }
        }
    }

    void swap(vector<int>& nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
};