// https://leetcode.com/problems/subarray-sums-divisible-by-k/

#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int subarraysDivByK(vector<int>& nums, int k) {

        unordered_map<int, int> mapResCounts;
        mapResCounts[0] = 1;

        int acc = 0;
        int count = 0;
        for (auto& num: nums) {
            acc += num;

            int res = (acc % k + k) % k; 

            if (mapResCounts.find(res) != mapResCounts.end()) {
                count += mapResCounts[res];
            } else {
                mapResCounts[res] = 0;
            }

            mapResCounts[res] += 1;
        }

        return count;
    }
};