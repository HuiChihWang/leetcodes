// https://leetcode.com/problems/flip-string-to-monotone-increasing/
// https://www.youtube.com/watch?v=tMq9z5k3umQ

#include <string>

using namespace std;

class Solution {
public:
    int minFlipsMonoIncr(string s) {
        int countOnes = 0;
        int minEdit = 0;

        for(int i = 0; i < s.length(); ++i) {
            if (s[i] == '1') {
                minEdit = min(minEdit, 1 + countOnes);
                ++countOnes;
            } else {
                minEdit = min(1 + minEdit, countOnes);
            }
        }
        
        return minEdit;
    }
};