// https://leetcode.com/problems/lru-cache/

#include <list>
#include <unordered_map>

using namespace std;

class LRUCache {
private:
    unordered_map<int, list<pair<int, int>>::iterator> cache;
    list<pair<int, int>> order;

    int maxCapacity = 0;
    
    void updateOrder(int key) {
        auto it = cache[key];
        int value = it->second;
        order.erase(it);
        order.push_front({key, value});
        cache[key] = order.begin();
    }

public:
    LRUCache(int capacity) {
        this->maxCapacity = capacity;
    }
    
    int get(int key) {
        if (cache.find(key) == cache.end()) {
            return -1;
        }

        updateOrder(key);

        return order.front().second;
    }
    
    void put(int key, int value) {
        if (cache.find(key) != cache.end()) {
            updateOrder(key);
            order.front().second = value;
            return;
        }

        order.push_front({key, value});
        cache[key] = order.begin();

        if (cache.size() > maxCapacity) {
            int keyToRemoved = order.back().first;
            order.pop_back();
            cache.erase(keyToRemoved);
        }
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */