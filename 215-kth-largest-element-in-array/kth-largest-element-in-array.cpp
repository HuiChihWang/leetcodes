// https://leetcode.com/problems/kth-largest-element-in-an-array/description/

#include <vector>
using namespace std;


class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        return findKthLargest(nums, 0, nums.size() - 1, k);
    }

    int findKthLargest(vector<int>& nums, int start, int end, int k) {
        int pivotIndex = partition(nums, start, end);

        int leftSize = pivotIndex - start + 1;

        if (k < leftSize) {
            return findKthLargest(nums, start, pivotIndex - 1, k);
        } else if (k > leftSize) {
            return findKthLargest(nums, pivotIndex + 1, end, k - leftSize);
        } else {
            return nums[pivotIndex];
        }

        return -1;
    }

    int partition(vector<int>& nums, int start, int end) {
        int pivotIndex = end;
        swap(nums, pivotIndex, end);
        int pivot = nums[end];

        int firstEnd = start - 1;
        
        for (int i = start; i < end; ++i) {
            if (nums[i] >= pivot) {
                swap(nums, i, ++firstEnd);
            }
        }

        swap(nums, pivotIndex, firstEnd + 1);

        return firstEnd + 1;
    }

    void swap(vector<int>& nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
};