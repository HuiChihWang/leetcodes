// https://leetcode.com/problems/decode-string/description/

#include <string>
using namespace std;

class Solution {
public:
    string decodeString(string s) {
        
        int prev = -1;
        string ret = "";
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '[') {
                int start = i + 1;
                int numberStart = prev + 1;
                int numberEnd = i - 1;

                int leftCount = 0;
                while (i < s.length()) {
                    if (s[i] == '[') {
                        leftCount += 1;
                    } else if (s[i] == ']') {
                        leftCount -= 1;
                    }

                    if (leftCount == 0) {
                        break;
                    }

                    ++i;
                }

                string pattern =  decodeString(s.substr(start, i - start));

                int number = stoi(s.substr(numberStart, numberEnd - numberStart + 1));

                for (int j = 0; j < number; ++j) {
                    ret += pattern;
                }

                prev = i;
            } else if (s[i] >= 'a' && s[i] <= 'z') {
                ret += s[i];
                prev = i;
            }
        }

        return ret;
    }
};