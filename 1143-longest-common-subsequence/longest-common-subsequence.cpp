// https://leetcode.com/problems/longest-common-subsequence/
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    int longestCommonSubsequence(string text1, string text2) {
        vector<vector<int>> maxLength(text1.length() + 1, vector<int>(text2.length() + 1, 0));

        for (int i = 0; i < text1.length(); ++i) {
            for (int j = 0; j < text2.length(); ++j) {
                if (text1[i] == text2[j]) {
                    maxLength[i + 1][j + 1] = 1 + maxLength[i][j];
                } else {
                    maxLength[i + 1][j + 1] = max(maxLength[i + 1][j], maxLength[i][j + 1]);
                }
            }
        }

        return maxLength[text1.length()][text2.length()];
    }
};