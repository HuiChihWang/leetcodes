// https://leetcode.com/problems/squares-of-a-sorted-array/description/

#include <vector>

using namespace std;

class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {
        vector<int> squares(nums.size());

        int left = 0, right = nums.size() - 1;
        int index = nums.size() - 1;
        while(left <= right) {
            if (abs(nums[right]) > abs(nums[left])) {
                squares[index] = nums[right] * nums[right];
                --right;
            } else {
                squares[index] = nums[left] * nums[left];
                ++left;
            }

            --index;
        }

        return squares;
    }
};