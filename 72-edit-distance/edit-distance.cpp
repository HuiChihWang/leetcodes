// https://leetcode.com/problems/edit-distance/

#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    int minDistance(string str1, string str2)
    {
        vector<vector<int>> minEdit(str1.length() + 1, vector<int>(str2.length() + 1, 0));

        for (int i = 0; i < str1.length(); ++i)
        {
            minEdit[i + 1][0] = i + 1;
        }
        for (int i = 0; i < str2.length(); ++i)
        {
            minEdit[0][i + 1] = i + 1;
        }

        for (int i = 0; i < str1.length(); ++i)
        {
            for (int j = 0; j < str2.length(); ++j)
            {

                int distByInsertion = 1 + minEdit[i][j + 1];
                int distByRemoval = 1 + minEdit[i + 1][j];
                int distBySub = minEdit[i][j];

                if (str1[i] != str2[j])
                {
                    distBySub += 1;
                }

                minEdit[i + 1][j + 1] = min(min(distByInsertion, distByRemoval), distBySub);
            }
        }

        return minEdit[str1.length()][str2.length()];
    }
};