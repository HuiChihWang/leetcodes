// https://leetcode.com/problems/swap-nodes-in-pairs/

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        if (head == nullptr || head->next == nullptr) {
            return head;
        }

        ListNode *newHead = head->next;
        ListNode  *prev = nullptr, *cur = head; 
        
        while (cur != nullptr && cur->next != nullptr) {
            ListNode *next = cur->next;
            
            if (prev != nullptr) {
                prev->next = next;
            }


            cur->next = next->next;
            next->next = cur;
            
            prev = cur;
            cur = cur->next;
        }

        return newHead;
    }
};