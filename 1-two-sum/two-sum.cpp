// https://leetcode.com/problems/two-sum/

#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
public:
    vector<int> twoSum(vector<int> &nums, int target)
    {
        unordered_map<int, int> visited;
        for (int i = 0; i < nums.size(); ++i) {
            int remaining = target - nums[i];
            if (visited.find(remaining) != visited.end()) {
                return {visited[remaining], i};
            }

            visited[nums[i]] = i;
        }

        return {};
    }
};