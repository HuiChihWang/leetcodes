// https://leetcode.com/problems/next-permutation/description/
#include <vector>

using namespace std;

class Solution
{
public:
    void nextPermutation(vector<int> &nums)
    {
        int start = nums.size() - 1;

        while (start >= 1 && nums[start - 1] >= nums[start])
        {
            --start;
        }

        if (start >= 1)
        {
            int firstLarger = -1;
            int left = start, right = nums.size() - 1;
            while (left <= right)
            {
                int mid = left + (right - left) / 2;

                if (nums[mid] > nums[start - 1])
                {
                    firstLarger = mid;
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            int temp = nums[firstLarger];
            nums[firstLarger] = nums[start - 1];
            nums[start - 1] = temp;
        }

        reverse(nums.begin() + start, nums.end());
    }
};