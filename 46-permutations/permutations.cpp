// https://leetcode.com/problems/permutations/
#include <vector>

using namespace std;

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> collected;
        vector<int> cache;
        vector<bool> visited(nums.size());

        collect(nums, visited, cache, collected);

        return collected;
    }

    void collect(const vector<int>& nums, vector<bool>& visited, vector<int>& cache, vector<vector<int>>& collected) {
        if (cache.size() == nums.size()) {
            collected.emplace_back(cache);
            return;
        }

        for (auto i = 0; i < nums.size(); ++i) {
            if (visited[i]) {
                continue;
            }

            cache.emplace_back(nums[i]);
            visited[i] = true;
            collect(nums, visited, cache, collected);
            cache.pop_back();
            visited[i] = false;
        }
    }
};