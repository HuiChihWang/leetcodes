// https://leetcode.com/problems/permutations-ii/
#include <vector>
#include <algorithm>
using namespace std;


class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<bool> visited(nums.size()); 
        vector<int> cache; 
        vector<vector<int>> collected;

        sort(nums.begin(), nums.end());
        collect(nums, visited, cache, collected);

        return collected;
    }

    void collect(const vector<int> nums, vector<bool>& visited, vector<int>& cache, vector<vector<int>>& collected) {
        if (cache.size() == nums.size()) {
            collected.emplace_back(cache);
            return;
        }

        for (int i = 0; i < nums.size(); ++i) {
            if (visited[i]) {
                continue;
            }

            if (i >= 1 && nums[i] == nums[i-1] && !visited[i-1]) {
                continue;
            }


            cache.emplace_back(nums[i]);
            visited[i] = true;
            collect(nums, visited, cache, collected);
            cache.pop_back();
            visited[i] = false;
        }
    }
};
