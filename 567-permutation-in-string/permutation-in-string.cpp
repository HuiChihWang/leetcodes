// https://leetcode.com/problems/permutation-in-string/description/
#include <vector>

using namespace std;

class Solution {
public:
    bool checkInclusion(string s1, string s2) {
        vector<int> s1Count(26, 0);

        for (auto& c: s1) {
            int index = (int) (c - 'a');
            ++s1Count[index];
        }
        
        vector<int> window(26, 0);
        int windowSize = s1.length();

        for (int i = 0; i < s2.length(); ++i) {
            int newIndex = (int) (s2[i] - 'a');
            window[newIndex] += 1;

            if (i >= windowSize) {
                int outIndex = (int) (s2[i - windowSize] - 'a');
                window[outIndex] -= 1;
            }

            if (window == s1Count) {
                return true;
            }
        }

        return false;
    }
};