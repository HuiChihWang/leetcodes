// https://leetcode.com/problems/minimum-number-of-vertices-to-reach-all-nodes/
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> findSmallestSetOfVertices(int n, vector<vector<int>>& edges) {
        vector<int> inDegrees(n, 0);
        for (auto& edge: edges) {
            inDegrees[edge[1]] += 1;
        }

        vector<int> starts;
        for (int i = 0; i < n; ++i) {
            if (inDegrees[i] == 0) {
                starts.push_back(i);
            }
        }
        
        return starts;
    }
};