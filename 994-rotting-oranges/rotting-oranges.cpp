// https://leetcode.com/problems/rotting-oranges/
#include <vector>
#include <queue>

using namespace std;

class Solution {
private: 
    const int dirs[4][2] = {
        {1, 0},
        {-1, 0},
        {0, 1},
        {0, -1},
    };

public:
    int orangesRotting(vector<vector<int>>& grid) {
        int rows = grid.size(), cols = grid[0].size();
        int time = 0;
        queue<pair<int, int>> bfsQueue;

        int freshCount = 0;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (grid[i][j] == 2) {
                    bfsQueue.push({i, j});
                } else if (grid[i][j] == 1) {
                    freshCount += 1;
                }
            }
        }
        
        while (!bfsQueue.empty() && freshCount != 0) {
            int qSize = bfsQueue.size();

            for (int i = 0; i < qSize; ++i) {
                auto top = bfsQueue.front();
                bfsQueue.pop();

                for (auto& [dr, dc]: dirs) {
                    int nextR = dr + top.first;
                    int nextC = dc + top.second;

                    if (nextR < 0 || nextR >= rows || nextC < 0 || nextC >= cols) {
                        continue;
                    }

                    if (grid[nextR][nextC] == 2 || grid[nextR][nextC] == 0) {
                        continue;
                    } 

                    grid[nextR][nextC] = 2;
                    freshCount -= 1;
                    bfsQueue.push({nextR, nextC});
                }
            }

            time += 1;
        }

        return freshCount == 0 ? time : -1;
    }
};