// https://leetcode.com/problems/sort-the-matrix-diagonally/
#include <vector>

using namespace std;

class Solution {
public:
    vector<vector<int>> diagonalSort(vector<vector<int>>& mat) {
        int rows = mat.size();
        int cols = mat[0].size();
        
        for (int startR = 0; startR < rows; ++startR) {
            int lengthOfDiagonal = getLengthOfDiagonal(startR, 0, rows, cols);
            sortDiagonal(mat, startR, 0, 0, lengthOfDiagonal - 1);
        }

        for (int startC = 1; startC < cols; ++startC) {
            int lengthOfDiagonal = getLengthOfDiagonal(0, startC, rows, cols);
            sortDiagonal(mat, 0, startC, 0, lengthOfDiagonal - 1);
        }
        
        return mat;
    }

    int getLengthOfDiagonal(int startR, int startC, int rows, int cols) {
        int endR = startR, endC = startC;
        int count = 0;
        while(endR < rows && endC < cols) {
            ++endR;
            ++endC;
            ++count;
        }
        return count;
    }

    void sortDiagonal(vector<vector<int>>& mat, int startR, int startC, int startIndex, int endIndex) {
        if (startIndex >= endIndex) {
            return;
        }

        int pivotIndex = partition(mat, startR, startC, startIndex, endIndex);

        sortDiagonal(mat, startR, startC, startIndex, pivotIndex - 1);
        sortDiagonal(mat, startR, startC, pivotIndex + 1, endIndex);
    }

    int partition(vector<vector<int>>& mat, int startR, int startC, int startIndex, int endIndex) {
        int pivotIndex = endIndex;
        swap(mat, startR + endIndex, startC + endIndex, startR + pivotIndex, startC + pivotIndex);
        int pivot = getNum(mat, startR, startC, endIndex);

        int leftIndex = startIndex - 1;
        for (int i = startIndex; i < endIndex; ++i) {
            if (getNum(mat, startR, startC, i) <= pivot) {
                ++leftIndex;
                swap(mat, startR + i, startC + i, startR + leftIndex, startC + leftIndex);
            }
        }

        pivotIndex = leftIndex + 1;
        swap(mat, startR + endIndex, startC + endIndex, startR + pivotIndex, startC + pivotIndex);
        return pivotIndex;
    }


    int getNum(vector<vector<int>>& mat, int startR, int startC, int index) {
        return mat[startR + index][startC + index];
    }

    void swap(vector<vector<int>>& mat, int r1, int c1, int r2, int c2) {
        int temp = mat[r1][c1];
        mat[r1][c1] = mat[r2][c2];
        mat[r2][c2] = temp;
    }
};