// https://leetcode.com/problems/binary-tree-paths/description/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> paths;
        collectPath(root, "", paths);
        return paths;
    }

    void collectPath(TreeNode* root, string path, vector<string>& paths) {
        if (root == nullptr) {
            return;
        }

        path += to_string(root->val);

        if (root->left == nullptr && root->right == nullptr) {
            paths.emplace_back(path);
        }

        path += "->";
        collectPath(root->left, path, paths);
        collectPath(root->right, path, paths);
    }
};