// https://leetcode.com/problems/longest-word-in-dictionary-through-deleting/
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    string findLongestWord(string s, vector<string>& dictionary) {
        string wordWithMaxLength = "";

        for (const auto& word: dictionary) {
            if (word.length() < wordWithMaxLength.length()) {
                continue;
            }

            if (word.length() == wordWithMaxLength.length() && word > wordWithMaxLength) {
                continue;
            }

            if (isSubSequence(s, word)) {
                wordWithMaxLength = word;
            }
        }

        return wordWithMaxLength;
    }

    bool isSubSequence(const string& s, const string& substr) {
        int count = 0;

        for (const auto& c: s) {
            if (substr[count] == c) {
                ++count;
            }

            if (count == substr.length()) {
                return true;
            }
        }

        return false;
    } 
};