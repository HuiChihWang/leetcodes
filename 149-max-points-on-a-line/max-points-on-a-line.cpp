// https://leetcode.com/problems/max-points-on-a-line/

#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int maxPoints(vector<vector<int>>& points) {
        int maxPoints = 1;
        unordered_map<string, int> lines;

        for (int i = 0; i < points.size(); ++i) {
            const auto& p1 = points[i];
            // cout << p1[0] << " " << p1[1] << endl;
            for (int j = i + 1; j < points.size(); ++j) {
                const auto& p2 = points[j];

                auto slope = getSlope(p1, p2);
                auto slopeKey = getSlopeKey(slope);
                
                // cout << p2[0] << " " << p2[1] << " " <<slopeKey << endl;

                if (lines.find(slopeKey) == lines.end()) {
                    lines[slopeKey] = 1;
                }

                lines[slopeKey] += 1;
            }

            for (const auto& it: lines) {
                maxPoints = max(maxPoints, it.second);
            }
            // cout << endl;
            lines.clear();
        }
        return maxPoints;
    }

private:
    pair<int, int> getSlope(const vector<int>& p1, const vector<int>& p2) {
        int xDiff = p1[0] - p2[0];
        int yDiff = p1[1] - p2[1];

        if (xDiff == 0) {
            return {0, 1};
        } else if (yDiff == 0) {
            return {1, 0};
        }
        
        int divisor = getLargestCommonDivisor(xDiff, yDiff);

        xDiff /= divisor;
        yDiff /= divisor;

        if (xDiff < 0) {
            xDiff *= -1;
            yDiff *= -1;
        }

        return {xDiff, yDiff};
    }

    string getSlopeKey(const pair<int, int>& slope) {
        return to_string(slope.first) + " " + to_string(slope.second);
    }

    int getLargestCommonDivisor(int x1, int x2) {
        int num = abs(x1);
        int divisor = abs(x2);

        while (divisor != 0) {
            int res = num % divisor;
            num = divisor;
            divisor = res;
        }

        return num;
    }
};