// https://leetcode.com/problems/split-array-largest-sum/
#include <vector>

using namespace std;

class Solution {
public:
    int splitArray(vector<int>& nums, int k) {
        int maxInArr = numeric_limits<int>::min();
        int sumArr = 0;

        for (auto& num: nums) {
            sumArr += num;
            maxInArr = max(maxInArr, num);
        }

        int left = maxInArr, right = sumArr;

        int minimumSum = 0;
        while (left <= right) {
            int maxSum = left + (right - left) / 2;

            if (canBeSplited(nums, maxSum, k)) {
                minimumSum = maxSum;
                right = maxSum - 1;
            } else {
                left = maxSum + 1;
            }
        }

        return minimumSum;

    }

    bool canBeSplited(const vector<int>& nums, int maxSum, int k) {
        int sum = 0;
        int count = 0;

        for (const auto& num: nums) {
            if (num + sum > maxSum) {
                ++count;
                sum = 0;
            }

            sum += num;
        }

        ++count;

        return count <= k;
    }
};