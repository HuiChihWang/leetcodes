// https://leetcode.com/problems/single-element-in-a-sorted-array/
#include <vector>

using namespace std;

class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        if (nums.size() == 1) {
            return nums[0];
        }

        int left = 1; 
        int right = nums.size() - 2;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (nums[mid] != nums[mid - 1] && nums[mid] != nums[mid + 1]) {
                return nums[mid];
            }

            int first = mid;
            int second = mid;
            if (nums[mid] == nums[mid - 1]) {
                first = mid - 1;
            } else {
                second = mid + 1;
            }

            int leftSize = first;
            int rightSize = nums.size() - 1 - second;

            if (leftSize % 2 != 0) {
                right = first - 1;
            } else {
                left = second + 1;
            }
        }

        return nums[0] != nums[1] ? nums[0] : nums.back();
    }
};