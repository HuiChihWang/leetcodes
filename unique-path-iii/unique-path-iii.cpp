// https://leetcode.com/problems/unique-paths-iii/

#include <vector>
using namespace std;

class Solution
{
private:
    const int dirs[4][2] = {
        {1, 0},
        {0, 1},
        {-1, 0},
        {0, -1},
    };

public:
    int uniquePathsIII(vector<vector<int>> &grid)
    {
        auto ret = findStartPosAndNumberOfEmptyCell(grid);

        int startR = ret[0], startC = ret[1];
        int totalEmpty = ret[2];

        grid[startR][startC] = 0;
        totalEmpty += 1;

        int totalWays = walk(grid, startR, startC, totalEmpty);

        return totalWays;
    }

    int walk(vector<vector<int>> &grid, int r, int c, int totalEmpty)
    {
        if (!isInBound(grid, r, c) || grid[r][c] == -1)
        {
            return 0;
        }

        if (grid[r][c] == 2)
        {
            return totalEmpty == 0;
        }

        grid[r][c] = -1;
        totalEmpty -= 1;

        int ways = 0;
        for (auto &[dr, dc] : dirs)
        {
            int nextR = r + dr, nextC = c + dc;
            ways += walk(grid, nextR, nextC, totalEmpty);
        }

        grid[r][c] = 0;

        return ways;
    }

    vector<int> findStartPosAndNumberOfEmptyCell(const vector<vector<int>> &grid)
    {
        vector<int> ret(3);
        int totalEmpty = 0;

        for (int i = 0; i < grid.size(); ++i)
        {
            for (int j = 0; j < grid[0].size(); ++j)
            {
                if (grid[i][j] == 1)
                {
                    ret[0] = i;
                    ret[1] = j;
                }

                if (grid[i][j] == 0)
                {
                    totalEmpty += 1;
                }
            }
        }

        ret[2] = totalEmpty;

        return ret;
    }

    bool isInBound(const vector<vector<int>> &grid, int r, int c)
    {
        return r >= 0 && r < grid.size() && c >= 0 && c < grid[0].size();
    }
};